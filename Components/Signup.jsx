import {
  Avatar,
  Box,
  Button,
  Flex,
  FormControl,
  Heading,
  Input,
  InputGroup,
  InputLeftElement,
  InputRightElement,
  Link,
  Stack,
  chakra,
  useToast,
} from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import { FaUserAlt, FaLock } from "react-icons/fa";
import { AiTwotoneMail } from "react-icons/ai";
import { useNavigate } from "react-router-dom";
import { logEvent } from "firebase/analytics";
import { analytics } from "../firebase-config";
import { Helmet } from "react-helmet";
const CFaUserAlt = chakra(FaUserAlt);
const CFaLock = chakra(FaLock);
import { Client, Account, ID, Databases } from "appwrite";
const CAiTwotoneMail = chakra(AiTwotoneMail);

const Signup = () => {
  useEffect(() => {
    logEvent(analytics, "page_view");
    const UID = localStorage.getItem("UID");
    if (UID) {
      navigate("/scan", { replace: true });
    }
  }, []);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [enrollmentNo, setEnrollmentNo] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [loading, setLoading] = useState(false);
  const handleShowClick = () => setShowPassword(!showPassword);
  const toast = useToast();
  const navigate = useNavigate();
  const headers1 = {
    "Content-Type": "application/json",
    "X-Appwrite-Project": "647a0a038994b45647f7",
    "X-Appwrite-Key":
      "0cd67747b711b8c0bdb2987136fc531989e89cc36fd9e802f3c40af69f2e2b5f4221827d997037419ab44aa70cd7b899b1d57a138fbd28c67e8f32cc5479ec87dfb73a7d26af587fb1d3ae006679897e2d0437cd697106314aa8b6f9a5b5b17226b07c1200fc7a2dc45ea036db66ff6dfc3ef4a33b2d1dc7fce0dcca991aabe9",
  };
  const headers = {
    "Content-Type": "application/json",
    "X-Appwrite-Project": "647a0a038994b45647f7",
  };
  const handleSignup = (e) => {
    if (!email || !password || !name || !enrollmentNo) {
      return;
    }
    e.preventDefault();
    setLoading(true);
    const client = new Client();
    const account = new Account(client);
    const databases = new Databases(client);
    client.setEndpoint("https://cloud.appwrite.io/v1");
    client.setProject(import.meta.env.VITE_APPWRITE_PROJECT_ID);

    const promise = account.create(ID.unique(), email, password, name);
    promise
      .then(function (response) {
        localStorage.setItem("UID", response.$id);
        const prommise = databases.createDocument(
          import.meta.env.VITE_APPWRITE_DATABASE_ID,
          import.meta.env.VITE_APPWRITE_USERS_COLLECTION_ID,
          ID.unique(),
          {
            name: name,
            email: email,
            enrollmentno: enrollmentNo,
            uid: response.$id,
          }
        );
        prommise
          .then(function (response) {
            toast({
              title: "Signup successful!",
              description: "Account created successfully!",
              status: "success",
              duration: 4000,
              isClosable: true,
            });
            navigate("/scan", { replace: true });
            setEmail("");
            setPassword("");
            setName("");
            setEnrollmentNo("");
          })
          .catch(function (error) {
            toast({
              title: "An error occurred.",
              description: error.message,
              status: "error",
              duration: 4000,
              isClosable: true,
            });
          });
      })
      .catch(function (error) {
        toast({
          title: "An error occurred.",
          description: "A user with the same email already exists.",
          status: "error",
          duration: 4000,
          isClosable: true,
        });
      });

    setLoading(false);
  };
  return (
    <>
      <Helmet>
        {`
        <script
        async
        src="https://www.googletagmanager.com/gtag/js?id=G-CM3SWTD04Z"
      ></script>
      <script>
        window.dataLayer = window.dataLayer || []; function gtag()
        {dataLayer.push(arguments);}
        gtag('js', new Date()); gtag('config', 'G-CM3SWTD04Z');
      </script>
        `}
      </Helmet>
      <Flex
        flexDirection="column"
        width="100wh"
        height="100vh"
        backgroundColor="gray.200"
        justifyContent="center"
        alignItems="center"
      >
        <Stack
          flexDir="column"
          mb="2"
          justifyContent="center"
          alignItems="center"
        >
          <Avatar bg="teal.500" />
          <Heading color="teal.400">Welcome</Heading>
          <Box minW={{ base: "90%", md: "468px" }}>
            <form>
              <Stack
                spacing={4}
                p="1rem"
                backgroundColor="whiteAlpha.900"
                boxShadow="md"
              >
                <FormControl isRequired>
                  <InputGroup>
                    <InputLeftElement
                      pointerEvents="none"
                      children={<CFaUserAlt color="gray.300" />}
                    />
                    <Input
                      type="text"
                      placeholder="Name"
                      onChange={(e) => {
                        setName(e.target.value);
                      }}
                    />
                  </InputGroup>
                </FormControl>
                <FormControl isRequired>
                  <InputGroup>
                    <InputLeftElement
                      pointerEvents="none"
                      children={<CAiTwotoneMail color="gray.300" />}
                    />
                    <Input
                      type="email"
                      placeholder="Email address"
                      onChange={(e) => {
                        setEmail(e.target.value);
                      }}
                    />
                  </InputGroup>
                </FormControl>
                <FormControl isRequired>
                  <InputGroup>
                    <InputLeftElement
                      pointerEvents="none"
                      color="gray.300"
                      children={<CFaLock color="gray.300" />}
                    />
                    <Input
                      type={showPassword ? "text" : "password"}
                      placeholder="Password"
                      onChange={(e) => {
                        setPassword(e.target.value);
                      }}
                    />
                    <InputRightElement width="4.5rem">
                      <Button h="1.75rem" size="sm" onClick={handleShowClick}>
                        {showPassword ? "Hide" : "Show"}
                      </Button>
                    </InputRightElement>
                  </InputGroup>
                </FormControl>
                <FormControl isRequired>
                  <InputGroup>
                    <InputLeftElement
                      pointerEvents="none"
                      children={<CFaUserAlt color="gray.300" />}
                    />
                    <Input
                      type="number"
                      placeholder="Enrollment Number"
                      onChange={(e) => {
                        setEnrollmentNo(e.target.value);
                      }}
                    />
                  </InputGroup>
                </FormControl>
                <Button
                  borderRadius={0}
                  type="submit"
                  variant="solid"
                  colorScheme="teal"
                  width="full"
                  onClick={handleSignup}
                  isLoading={loading}
                >
                  Sign Up
                </Button>
              </Stack>
            </form>
          </Box>
        </Stack>
        <Box>
          Already a User?{" "}
          <Link
            color="teal.500"
            onClick={() => {
              navigate("/", { replace: true });
            }}
          >
            Login
          </Link>
        </Box>
      </Flex>
    </>
  );
};

export default Signup;
