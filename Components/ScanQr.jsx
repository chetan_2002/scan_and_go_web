import { Box, Text, useToast } from "@chakra-ui/react";

import React, { useEffect, useState } from "react";
import { QrReader } from "react-qr-reader";
import Lottie from "react-lottie";
import animationData from "../assets/success.json";
import { useNavigate } from "react-router-dom";
import { logEvent } from "firebase/analytics";
import { analytics } from "../firebase-config";
import { Helmet } from "react-helmet";
import { Client, Databases, ID, Query } from "appwrite";

const ScanQr = () => {
  const [data, setData] = useState(null);
  const [scan, setScan] = useState(true);
  const toast = useToast();
  const navigate = useNavigate();
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };
  useEffect(() => {
    if (localStorage.getItem("UID") === null) {
      navigate("/", { replace: true });
    }
    const last_scan_time = localStorage.getItem("last_scan_time");
    var time_difference =
      (new Date().getTime() - new Date(last_scan_time).getTime()) / 1000;
    time_difference = time_difference / 60;
    time_difference = Math.round(time_difference);
    if (time_difference < 45) {
      toast({
        title: "You can scan QR code only once in 45 minutes.",
        description:
          "Please try again after " + (45 - time_difference) + " minutes.",
        status: "error",
        duration: 4000,
        isClosable: true,
      });
      return;
    }
    if (data !== null) {
      localStorage.setItem("last_scan_time", new Date().toUTCString());
      const uid = localStorage.getItem("UID");
      const client = new Client();
      const databases = new Databases(client);
      client.setEndpoint("https://cloud.appwrite.io/v1");
      client.setProject(import.meta.env.VITE_APPWRITE_PROJECT_ID);

      const promise = databases.listDocuments(
        import.meta.env.VITE_APPWRITE_DATABASE_ID,
        import.meta.env.VITE_APPWRITE_USERS_COLLECTION_ID,
        [Query.equal("uid", [uid])]
      );
      promise
        .then(function (response) {
          const promise1 = databases.createDocument(
            import.meta.env.VITE_APPWRITE_DATABASE_ID,
            import.meta.env.VITE_APPWRITE_ATTENDANCE_COLLECTION_ID,
            ID.unique(),
            {
              name: response.documents[0].name,
              email: response.documents[0].email,
              enrollmentno: response.documents[0].enrollmentno,
              date: new Date().toUTCString(),
              time: new Date().toLocaleTimeString(),
              collegeName: data.toUpperCase(),
            }
          );
          promise1
            .then(function (response) {
              toast({
                title: "Attendance Marked Successfully",
                status: "success",
                duration: 2000,
                isClosable: true,
              });
            })
            .catch(function (error) {
              toast({
                title: "There was an error while marking attendance",
                status: "error",
                duration: 2000,
                isClosable: true,
              });
            });
        })
        .catch(function (error) {
          toast({
            title: "There was an error while marking attendance",
            status: "error",
            duration: 2000,
            isClosable: true,
          });
        });
      setScan(false);
    }
  }, [data]);

  return (
    <>
      <Helmet>
        {`
          <script
          async
          src="https://www.googletagmanager.com/gtag/js?id=G-CM3SWTD04Z"
        ></script>
        <script>
          window.dataLayer = window.dataLayer || []; function gtag()
          {dataLayer.push(arguments);}
          gtag('js', new Date()); gtag('config', 'G-CM3SWTD04Z');
        </script>
          `}
      </Helmet>
      <Box
        style={{
          display: "flex",
          flexDirection: "column",
        }}
      >
        <Box
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            paddingTop: "6px",
            paddingBottom: "6px",
            fontSize: "1.5rem",
            backgroundColor: "teal",
            color: "white",
            fontWeight: "bold",
          }}
        >
          <Text>Scan QR</Text>
        </Box>
        <Box
          marginTop={{ base: "8rem", md: "0" }}
          // display={{ lg: "none" }}
          // width={{ lg: "70%" }}

          width={"100%"}
        >
          {scan === true ? (
            <>
              <QrReader
                constraints={{ facingMode: "environment" }}
                onResult={(result, error) => {
                  if (result) {
                    setData(result?.text);
                  }
                }}
              />
            </>
          ) : (
            <>
              <Lottie options={defaultOptions} height={400} width={400} />
            </>
          )}
        </Box>
      </Box>
    </>
  );
};

export default ScanQr;
